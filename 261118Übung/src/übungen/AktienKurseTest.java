package �bungen;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class AktienKurseTest {

	@Test
	void testAktienRechner() throws IOException {
		assertEquals(44.54, AktienKurse.AktienRechner("CSCO"), 0.1);
		assertEquals(189.10, AktienKurse.AktienRechner("GS"), 0.1);
	}

}
