import java.util.ArrayList;

public class Hochschulverwaltung {
	ArrayList<Person> personenListe = new ArrayList<>();

	public void addStudent (String name,String vorname,String Studiengang,int Matrikelnummer) {
		Student s1 =new Student(name,vorname,Studiengang,Matrikelnummer);
		personenListe.add(s1);
	}
	public void addProfessor (String name,String vorname,String Fachgebiet,String Büro) {
		Professor s1 =new Professor(name,vorname,Fachgebiet,Büro);
		personenListe.add(s1);
	}
	public ArrayList<Person> suche(String name) {
		ArrayList<Person> PersonG= new ArrayList<>();
		for(Person p :personenListe) {
			if(p.getName().toLowerCase().contains(name.toLowerCase()) || p.getVorname().toLowerCase().contains(name.toLowerCase())) {
				PersonG.add(p);
			}


		}
		return PersonG;
	}
}