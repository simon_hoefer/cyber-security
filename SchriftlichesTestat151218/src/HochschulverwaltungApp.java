import java.util.ArrayList;
import java.util.Scanner;

public class HochschulverwaltungApp {

	public static void main(String[] args) {
		Hochschulverwaltung h = new Hochschulverwaltung();
		h.addProfessor("Hasuke","Maxi","Automatisierungstechnik","L1001");
		h.addProfessor("Paulus","Peter","CSB","A201");
		h.addProfessor("Föller-Nord","Miriam","EI","A306");
		h.addStudent("Bahadori","Nadine","CSB",145789);
		h.addStudent("Sattler","Robert","CSB",1647912);
		h.addStudent("Vögeli","Tim","CSB",4697132);


		Scanner sc = new Scanner(System.in);
		System.out.println("Geben Sie den Namen ein den Sie suchen!");
		String userEingabe = sc.nextLine();


		ArrayList<Person> suchEingabe = h.suche(userEingabe);
		for(Person p : suchEingabe) {
			System.out.println(p);
		}
	}

}
