package pr1nachhilfe;
public class FindeWert {
    public static void main(String arg[]){


        int[][] meineTabelle = new int[][]{
            {   0,  1,  2,  3,  4,  5,  6,  7,  8,  9   },
            {   10, 11, 12, 13, 14, 15, 16, 17, 18, 19  },
            {   20, 21, 22, 23, 24, 25, 26, 27, 28, 29  }
        };


        String ergebniss = findePositionVonWert(meineTabelle, 10);


        System.out.println("\n\n\n");
        System.out.println(ergebniss);
        System.out.println("\n\n\n");

    }


    private static String findePositionVonWert(int[][] tabelle, int gesuchterWert){
        int aktuelleZeile = 0;

        for(int[] zeile: tabelle){
            int aktuelleSpalte = 0;
            for(int wert: zeile){
                if(wert == gesuchterWert){
                    return "Wert gefunden in Zeile "+(aktuelleZeile+1)+" und Spalte "+(aktuelleSpalte+1) ;
                }
                aktuelleSpalte++;
            }
            aktuelleZeile++;
        }
        return "nicht gefunden";
    }
}

