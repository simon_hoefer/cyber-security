
public class Stack {
	//Attribute
	private int[] stackArray;
	private int index = -1;

	public Stack(int groesseArray) {
		stackArray= new int[groesseArray];
	}
	//Methoden
	public void DraufPacken(int zahl)throws RuntimeException {

		if(this.stackArray.length==AnzahlElemente()) {
			 throw new RuntimeException("Stack ist bereits voll!");
		}
		else {
			index++;
			this.stackArray[index] = zahl;
		}
	}
	public int RunterHolen() throws RuntimeException{

		if(StatusStackLeer()) {
			throw new RuntimeException("Stack ist bereits leer!");
		}
		else {
			int ausgabeZahl = this.stackArray[index];
			index--;
			return ausgabeZahl;
		}

	}
	//Überprüfung
	public boolean StatusStackLeer() {
		if(index<0) {
			return true;
		}
		else 
			return false;
	}
	public int AnzahlElemente() {
		return index +1;
	}
}