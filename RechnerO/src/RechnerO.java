import java.util.Scanner;

public class RechnerO {
	public static void main(String args[]) {
		Parameter zahlen = new Parameter();
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine Zahl ein: ");
		zahlen.p1 = Double.parseDouble(sc.nextLine());
		
		System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
		zahlen.p2 = Double.parseDouble(sc.nextLine());					//Eingabe
		
		Ergebnis erg = rechne(zahlen);
		
		System.out.println("Die Summe ist " + erg.summe + ", das Produkt ist: "  + erg.produkt); //Ausgabe
		sc.close();
	}
	
	public static Ergebnis rechne(Parameter p)
	{
		Ergebnis e = new Ergebnis();
		e.summe = (int) (p.p1 + p.p2);				//Verarbeitung
		e.produkt = (int) (p.p1 * p.p2);
		
		return e;
	}
	

}
