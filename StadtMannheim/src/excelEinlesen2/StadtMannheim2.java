package excelEinlesen2;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

public class StadtMannheim2 {

	public static void main(String[] args) throws FileNotFoundException{
		
		ArrayList<String> jahre = new ArrayList<>();
		int anzahlj13 = 0, anzahlj14 = 0,anzahlj15 = 0,anzahlj16 = 0,anzahlj17 = 0;
		Scanner sc = new Scanner(new File("./Quellen/daten.csv"));
		boolean nochMehr = true;
		
		while (nochMehr) {
	
		String inputFile = sc.nextLine();
		String[] zeile = inputFile.split(";");
		jahre.add(zeile[1]);
		nochMehr = sc.hasNext();
	}
		for(String jahr:jahre) {
			switch (jahr) {
				case "2017":anzahlj17++;break;
				case "2016":anzahlj16++;break;
				case "2015":anzahlj15++;break;
				case "2014":anzahlj14++;break;
				case "2013":anzahlj13++;break;
				default:break;
			}
		}

		System.out.println("2017 ist "+anzahlj17+" enthalten");
		System.out.println("2016 ist "+anzahlj16+" enthalten");
		System.out.println("2015 ist "+anzahlj15+" enthalten");
		System.out.println("2014 ist "+anzahlj14+" enthalten");
		System.out.println("2013 ist "+anzahlj13+" enthalten");
		sc.close();
	
	
	}

}
