
public class Student extends Person {
	public String Studiengang;
	public int Matrikelnummer;

	public Student(String name, String vorname, String Studiengang, int Matrikelnummer) {
		super(name,vorname);
		this.Studiengang=Studiengang;
		this.Matrikelnummer=Matrikelnummer;

	}
	public String toString() {
		return super.toString() + " Rolle : Student , Studiengang: " + this.Studiengang +", Matrikelnummer: " + this.Matrikelnummer;
	}
}
