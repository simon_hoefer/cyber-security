package �bungsaufgaben;

import java.util.Scanner;

public class Bu�geldrechnerA {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Wo sind Sie zu schnell gefahren?");
		String OrtsVariable = sc.nextLine();
		System.out.println("Wie viel km/h sind Sie zu gefahren?");
		int GeschwindigkeitsVariable = sc.nextInt();
		System.out.println("Wie viel km/h waren erlaubt?");
		int Erlaubt = sc.nextInt();
		Rechner(GeschwindigkeitsVariable,Erlaubt, OrtsVariable);
		sc.close();
	}
	public static void Rechner (int gefahreneGeschwindigkeit, int zul�ssigeGeschwindigkeit,String OrtsVariable) {
		
		int �berschritten = gefahreneGeschwindigkeit - zul�ssigeGeschwindigkeit;
		int Bu�geld=0;
		
		int[][]x = new int[10][4];
		x[0]=new int[] {1,10,15,10};
		x[1]=new int[] {11,15,25,20};
		x[2]=new int[] {16,20,35,30};
		x[3]=new int[] {21,25,80,70};
		x[4]=new int[] {26,30,100,80};
		x[5]=new int[] {31,40,160,120};
		x[6]=new int[] {41,50,200,160};
		x[7]=new int[] {51,60,280,240};
		x[8]=new int[] {61,70,480,440};
		x[9]=new int[] {71,Integer.MAX_VALUE,680,600};
		/*Stelle 1 vom Array is die mindestgeschwindigkeit, Stelle 2 die Max Geschwindigkeit, Stelle 3 Bu�geldinnerorts und Stelle 4 Bu�geldauserorts*/
		
		for(int[]zeile:x) {
			int minimum = zeile [0];
			int maximum = zeile [1];
			int innerortsB = zeile[2];
			int au�erortsB = zeile[3];
			
			if(�berschritten>=minimum&&�berschritten<=maximum) {
				if (OrtsVariable.toLowerCase().equals("innerorts")) {
					Bu�geld=innerortsB;
					
				}
				else if (OrtsVariable.toLowerCase().equals("au�erorts")) {
					Bu�geld=au�erortsB;
				}
				else { System.out.println("Ung�ltige Eingabe!");
				System.exit(-1);
				}
					
			}	
		
		}
		System.out.println("Sie m�ssen " + Bu�geld + "� bezahlen");
		
	}
}
