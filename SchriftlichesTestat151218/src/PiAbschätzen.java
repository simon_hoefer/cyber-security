
public class PiAbschätzen {
	static int AnzahlPfeile = 1000000;
	public static void main(String[] args) {
		System.out.println(Pfeile(AnzahlPfeile));
	}
	public static double Pfeile(int AnzahlPfeile) {
		int counter= 0;
		for (int i = 0;i<AnzahlPfeile;i++) {

			double xk = Math.random();
			double yk = Math.random();

			double treffer = Math.sqrt((xk*xk)+(yk*yk));
			
			if(treffer<=1) {
				counter++;
			}
		}
		double Pi=((double)counter/(double)AnzahlPfeile)*4;
		return Pi;

	}
}
