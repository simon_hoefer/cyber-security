package �bungen;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class HtmlEinlesen {

	public static void main(String[] args) throws IOException {

	    URL url = new URL("https://www.welt.de");

	    Scanner sc = new Scanner(url.openStream());

	    while (sc.hasNextLine()) {

	        System.out.println(sc.nextLine());

	    }

	    sc.close();

	}
}