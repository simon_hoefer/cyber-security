package pr1nachhilfe;

public class ZweiDemensionalesArray {

	public static void main(String[] args) {
		int[][]x = new int[4][4];
		x[0]=new int[] {1,10,15,10};
		x[1]=new int[] {11,15,25,20};	// erste [] entspricht zeile ,zweite [] entspricht spalte
		x[2]=new int[] {16,20,35,30};
		x[3]=new int[] {21,25,80,70};
		x[4]=new int[] {26,30,100,80};	}

}
