package pr1nachhilfe;

import java.util.ArrayList;

public class ForeachArrays {

	public static void main(String[] args) {
		int [] intarray = {2,5,6,1,18};
		ArrayList<Integer> arrayList = new ArrayList();
		for ( int zahl: intarray) {
			if (zahl > 2) {
				arrayList.add(zahl);
			}
		}

		for (int i :arrayList) {
			System.out.println(i);
		}
	}
}

