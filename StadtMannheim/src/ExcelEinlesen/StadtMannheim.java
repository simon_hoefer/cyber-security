
package ExcelEinlesen;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class StadtMannheim {

	public static void main(String[] args) throws FileNotFoundException {
		String dateiInhalt = ladeDatei("./Quellen/daten.csv");

		System.out.println(jahresZahlen(dateiInhalt));

	}
	public static String ladeDatei(String dateiName)throws FileNotFoundException {	
		Scanner sc = new Scanner(new File(dateiName));
		String zeilen = "";
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			System.out.println(line);
			zeilen = zeilen +"//"+ line;
		}
		sc.close();
		return zeilen;
	}
	public static int jahresZahlen(String datei) {
		int zaehler = 0;
		String[] zeilen = datei.split("//");
		for (int i = 2; i<zeilen.length;i++) {
			String [] worte = zeilen [i].split(";");

			if(worte[1].equals("2017")) {
				zaehler++;
			}
		}
		return zaehler;
	}


}

