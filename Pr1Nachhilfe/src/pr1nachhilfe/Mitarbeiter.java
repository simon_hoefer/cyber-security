package pr1nachhilfe;

public class Mitarbeiter {
	private String name;
	private String nachname;
	private int mnummer;
	private boolean gekündigt;


	public Mitarbeiter(String name, String nachname, int mnummer, boolean status) {
		this.name = name;
		this.nachname = nachname;
		this.mnummer = mnummer;
		this.gekündigt = gekündigt;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNachname() {
		return nachname;
	}


	public void setNachname(String nachname) {
		this.nachname = nachname;
	}


	public int getMnummer() {
		return mnummer;
	}


	public void setMnummer(int mnummer) {
		this.mnummer = mnummer;
	}


	public boolean isgekündigt() {
		return gekündigt;
	}


	public void setgekündigt(boolean gekündigt) {
		this.gekündigt= gekündigt;
	}
}