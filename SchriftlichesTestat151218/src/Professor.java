
public class Professor extends Person {
	public String Fachgebiet;
	public String Büro;
	
	public Professor(String name, String vorname,String Fachgebiet, String Büro) {
		super(name,vorname);
		this.Fachgebiet = Fachgebiet;
		this.Büro = Büro;
	}

	public String toString() {
		return super.toString() + "Rolle : Prof , Fachgebiet: " + this.Fachgebiet +", Büro: " + this.Büro;
	}

}
