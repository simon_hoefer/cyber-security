package �bungsaufgaben;
import java.util.ArrayList;
import java.util.Scanner;

public class ProgrammWh {

	//Aufgabe 3: noch eine Text-Analyse
	//Schreiben Sie eine Methode, die f�r einen �bergebenen Text ermittelt, welche Worte jeweils wie
	//oft in diesem Text vorkommen.
	//Vorgeschlagene Vorgehensweise
	//1. Erstellen Sie eine Klasse WortH�ufigkeit, die ein Wort und die H�ufigkeit seines Auftauchens
	//speichern kann
	//2. Verwenden Sie eine ArrayList<WortH�ufigkeit> in der Sie jedes neu auftauchende Wort
	//ablegen bzw. bei bereits enthaltenen Worten die H�ufigkeit erh�hen k�nnen
	//3. Zerlegen Sie den Ursprungstext in einzelne Worte (z.B. mit Hilfe der split-Methode)
	//4. Gehen Sie diese Worte nun der Reihe nach durch und speichern Sie jedes neu vorkommende
	//Wort in Ihrer ArrayList
	//o achten Sie darauf, dass jedes Wort nur genau einmal in die ArrayList kommt
	//o bei Worten, die bereits in der Liste sind, erh�hen Sie die H�ufigkeit
	//o erinnern Sie sich falls n�tig an das Auffinden einer Kontonummer in der BankingApp
	//in der Vorlesung

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte einen text eingeben!");
		String �bergebenderText = sc.nextLine();
		Z�hleW�rter(�bergebenderText);
	}
	public static ArrayList<WortHaeufigkeit> Z�hleW�rter(String �bergebenerText) {
		String �bergebeneWorte[] = �bergebenerText.split(" ");
		//System.out.println(Arrays.toString(worte));
		ArrayList<WortHaeufigkeit> listeGez�hlterWorte = new ArrayList<>();
		for(String �bergebenesWort:�bergebeneWorte) {
			for(WortHaeufigkeit wortgez�hlt :listeGez�hlterWorte) {
				if(wortgez�hlt.wort.equals(�bergebenesWort)) {
					wortgez�hlt.erh�heH�ufigkeit();
				}
				else {
					WortHaeufigkeit neuesGez�hltesWort = new WortHaeufigkeit(�bergebenesWort,1);
					listeGez�hlterWorte.add(neuesGez�hltesWort);
				}
			}
			if(listeGez�hlterWorte.isEmpty()) {
				WortHaeufigkeit neuesGez�hltesWort = new WortHaeufigkeit(�bergebenesWort,1);
				listeGez�hlterWorte.add(neuesGez�hltesWort);
			}

		}
		return listeGez�hlterWorte;

	}
}
